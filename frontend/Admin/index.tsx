import React from 'react'
import { BrowserRouter, Link, Switch, Route } from 'react-router-dom'

import { entities } from '../../server/db/entity'
import { RecordIndex } from './RecordIndex'
import { RecordUpdate } from './RecordUpdate'
import { RecordDelete } from './RecordDelete'
import { RecordCreate } from './RecordCreate'

type Props = {}
export const AdminRouter: React.FC<Props> = props => {
  return (
    <BrowserRouter>
      <div className="menu">
        {Object.keys(entities).map(entity => (
          <Link key={`admin-menu-${entity}`} to={`/admin/${entity}`}>
            {entity}
          </Link>
        ))}
      </div>
      <Switch>
        <Route path="/admin/:model/:id/delete">
          <RecordDelete />
        </Route>
        <Route path="/admin/:model/new">
          <RecordCreate />
        </Route>
        <Route path="/admin/:model/:id">
          <RecordUpdate />
        </Route>
        <Route path="/admin/:model">
          <RecordIndex />
        </Route>
        <Route path="/admin">
          <p>CRUD your records here!</p>
        </Route>
      </Switch>
    </BrowserRouter>
  )
}
