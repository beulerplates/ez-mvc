import React, { useEffect, useState } from 'react'
import { Fields, Record } from '../../server/db/entity'
import { useParams, Redirect } from 'react-router'
import Axios from 'axios'
import { HotInput } from '../common/HotInput'

export const RecordCreate = () => {
  const { model } = useParams()
  const [fields, setFields] = useState<Fields[]>([])
  const [status, setStatus] = useState<string | null>()
  const [record, setRecord] = useState<Partial<Record>>({})
  const [redirect, setRedirect] = useState(false)

  useEffect(() => {
    const getMetadata = async () => {
      const { relations, fields } = (
        await Axios.get<{ fields: Fields[]; relations: string[] }>(
          `/api/${model}/metadata`
        )
      ).data
      setFields(fields)
    }
    getMetadata()
  }, [model])

  const handleSave = async (e: React.FormEvent) => {
    e.preventDefault()
    await Axios.post(`/api/${model}`, record)
    setStatus('saved, redirecting to index!')
    setTimeout(() => setRedirect(true), 1000)
  }

  const updateRecord = (
    propertyName: string,
    val: number | string | Record
  ) => {
    setRecord({ ...record, ...{ [propertyName]: val } })
  }

  if (redirect) return <Redirect to={`/admin/${model}`} />
  if (!fields) <div>loading...</div>

  return (
    <div>
      {status && <p>{status}</p>}
      <h2>Create New {model}</h2>
      <form onSubmit={handleSave}>
        <p>TODO: Generatate fields based on schema</p>
        {fields.map(({ propertyName, type }, i) => {
          const value = record[propertyName as keyof Record] || ''
          if (propertyName === 'id')
            return <div key={`${model}-ID-${i}`}>Generated {propertyName}</div>
          return (
            <HotInput
              key={`entity-view-${model}-${propertyName}-${type}`}
              propertyName={propertyName}
              type={type || 'text'}
              value={value}
              updateRecord={updateRecord}
            />
          )
        })}
        <button type="submit">Create</button>
      </form>
    </div>
  )
}
