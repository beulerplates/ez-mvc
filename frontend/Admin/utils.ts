import Axios from 'axios'

export const getMetadata = async (model: string) => {
  const res = await Axios.get(`/api/${model}/metadta`)
  return res.data
}
