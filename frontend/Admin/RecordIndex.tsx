import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useParams } from 'react-router'
import { Fields, Record } from '../../server/db/entity'

import Axios from 'axios'
import { RecordShow } from './RecordShow'

export const RecordIndex = () => {
  const { model } = useParams()
  const [relations, setRelations] = useState<string[] | null>()

  const [fields, setFields] = useState<Fields[] | null>()
  const [records, setRecords] = useState<Record[] | null>()

  useEffect(() => {
    const getMetadata = async () => {
      const { data } = await Axios.get<{
        fields: Fields[]
        relations: string[]
      }>(`/api/${model}/metadata`)

      setRelations(data.relations)
      setFields(data.fields)
    }
    const get = async () => {
      const { data } = await Axios.get(`/api/${model}`)
      setRecords(data)
    }
    get()
    getMetadata()
  }, [model])

  if (!relations) return <h1 className="error">No Relations Found!</h1>
  if (!fields) return <h1 className="error">No Metadata Found!</h1>
  if (!records) return <h1 className="error">No Records Found!</h1>

  return (
    <div>
      <h3>All {model} records</h3>
      <Link to={`/admin/${model}/new`}>Create New</Link>
      {records.map((record, i) => (
        <RecordShow
          model={model as string}
          key={model + record.name + i}
          relations={relations}
          fields={fields}
          record={record}
        />
      ))}
    </div>
  )
}
