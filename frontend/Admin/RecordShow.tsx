import React from 'react'
import { Record, Fields } from '../../server/db/entity'
import './css/RecordShow.scss'
import { Link } from 'react-router-dom'
import { RecordRelation } from './RecordRelation'

type Props = {
  relations: string[]
  fields: Fields[]
  model: string
  record: Record
}

export const RecordShow: React.FC<Props> = ({
  relations,
  fields,
  model,
  record,
}) => {
  console.log(model, record, relations, fields)

  return (
    <div className="record-view">
      <h3>{name}</h3>
      {fields.map(({ propertyName }, i) => {
        const value = record[propertyName as keyof Record]

        if (typeof value !== 'object') {
          return (
            <p key={`record-show-${model}-${record.id}-${record.name}-${i}`}>
              {propertyName}: {record[propertyName as keyof Record]}
            </p>
          )
        } else {
        }
      })}

      {relations.map((relation, i) => (
        <RecordRelation
          key={`record-relation-${i}-${relation}-${record.id}`}
          relation={record[relation as keyof Record] as Record | Record[]}
          type={relation}
        />
      ))}
      <Link to={`/admin/${model}/${record.id}`}>
        <button>Edit</button>
      </Link>
      <Link to={`/admin/${model}/${record.id}/delete`}>
        <button>Delete</button>
      </Link>
    </div>
  )
}
