import React, { useState } from 'react'
import { useParams, Redirect } from 'react-router'

import { Link } from 'react-router-dom'
import Axios from 'axios'

export const RecordDelete = () => {
  // TODO: refactor this into a modal. makes more sense
  const { model, id } = useParams()
  const [deleted, setDeleted] = useState(false)
  const [status, setStatus] = useState<string | null>()

  const handleDelete = async () => {
    await Axios.delete(`/api/${model}/${id}`)
    setStatus('deleted, redirecting to index!')
    setTimeout(() => setDeleted(true), 1000)
  }

  if (deleted) return <Redirect to={`/admin/${model}?message=thing deleted!`} />

  return (
    <div>
      {status && <p>{status}</p>}
      <h3>Are you sure you want to delete {id}</h3>

      <button onClick={handleDelete}>yes</button>
      <Link to={`/admin/${model}`}>
        <button>no</button>
      </Link>
    </div>
  )
}
