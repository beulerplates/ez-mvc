import React from 'react'
import { Record } from '../../server/db/entity'

type Props = {
  relation: Record | Record[] | undefined
  type: string
}

export const RecordRelation: React.FC<Props> = ({ type, relation }) => {
  if (!relation) return <div>Loading...</div>

  return (
    <div className="relation-view">
      <p className="label">{type}</p>
      {Array.isArray(relation) ? (
        relation.map((record, i) => (
          <p key={`relation-text-${type}-${i}-${record.name}`} className="text">
            {record.name}
          </p>
        ))
      ) : (
        <p key={`relation-text-${type}-${relation.name}`} className="text">
          {relation.name}
        </p>
      )}
    </div>
  )
}
