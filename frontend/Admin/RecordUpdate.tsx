import React, { useState, useEffect } from 'react'
import { useParams, Redirect } from 'react-router'
import Axios from 'axios'
import { ModelTypes } from '../../server/db'
import { Fields, Record } from '../../server/db/entity'
import { HotInput } from '../common/HotInput'
import { HotSelect } from '../common/HotSelect'
import { HotRelation } from '../common/HotRelation'

export const RecordUpdate = () => {
  const { model, id } = useParams()
  const [record, setRecord] = useState<Record>({ id: 0, name: 'test' })
  const [status, setStatus] = useState<string | null>()
  const [fields, setFields] = useState<Fields[]>([])
  const [relations, setRelations] = useState<string[]>([])
  const [enums, setEnums] = useState<{ [id: string]: string[] }>({})
  const [redirect, setRedirect] = useState(false)

  const handleSave = async (e: React.MouseEvent) => {
    e.preventDefault()

    await Axios.put(`/api/${model}/${id}`, record)
    setStatus('saved, redirecting to index!')
    setTimeout(() => setRedirect(true), 1000)
  }

  useEffect(() => {
    const get = async () => {
      const {
        data: { fields, relations, enums },
      } = await Axios.get(`/api/${model}/metadata`)
      setFields(fields)
      setRelations(relations)
      setEnums(enums)

      const { data } = await Axios.get(`/api/${model}/${id}`)
      setRecord(data)
    }
    get()
  }, [id, model])
  if (!record || !fields.length) return <div>Loading...</div>

  const updateRecord = (
    propertyName: string,
    val: number | string | Record
  ) => {
    setRecord({ ...record, ...{ [propertyName]: val } })
  }

  if (redirect) return <Redirect to={`/admin/${model}`} />
  if (!record.id) return <div>Loading...</div>

  console.log('To Update =>', record, fields, relations)

  return (
    <div>
      {status && <p>{status}</p>}
      <h3>
        {model} - {record.name}
      </h3>

      <form>
        {fields.map(({ comment, type, propertyName }, i) => {
          const value = record[propertyName as keyof Record] as
            | number
            | string
            | Record
          if (comment === 'generated') {
            return (
              <div key={`generated-${propertyName} + ${i}`}>
                Generated {propertyName}: {value}
              </div>
            )
          } else if (comment === 'enum') {
            return (
              <HotSelect
                label={propertyName}
                key={`enum-${propertyName}-${value}`}
                options={enums[propertyName]}
                value={value as number}
                propertyName={propertyName}
                updateRecord={updateRecord}
              />
            )
          } else if (type) {
            return (
              <HotInput
                key={propertyName + i}
                type={type || 'text'}
                value={value as string | number}
                updateRecord={updateRecord}
                propertyName={propertyName}
              />
            )
          } else if (relations.includes(propertyName)) {
            return (
              <HotRelation
                key={`relation-${propertyName} + ${i}`}
                label={propertyName}
                relationModel={propertyName}
                record={value as Record}
                updateRecord={updateRecord}
              />
            )
          } else <div>IDK WHHAT TO DO {propertyName}</div>
        })}

        {relations.map((relation, i) => (
          <div key={`check-relation-${relation}-${i}`}>
            TODO: Gen relations for => {relation}
          </div>
        ))}
        <button onClick={handleSave}>Save!</button>
      </form>
    </div>
  )
}
