import React, { ChangeEvent } from 'react'

type Props = {
  options: string[]
  value: number
  label: string
  propertyName: string
  updateRecord: (field: string, value: number) => void
}
export const HotSelect: React.FC<Props> = ({
  options,
  value,
  label,
  updateRecord,
  propertyName,
}) => {
  const handleChange = (e: ChangeEvent<HTMLSelectElement>) => {
    updateRecord(propertyName, parseInt(e.target.value))
  }

  return (
    <div>
      <label>{label}</label>
      <select onChange={handleChange} value={value}>
        {options.map((opt, i) => (
          <option key={`option-${value}-${opt}-${i}`} value={i}>
            {opt}
          </option>
        ))}
      </select>
    </div>
  )
}
