import React, { ChangeEvent } from 'react'

type Props = {
  type: string
  value: number | string | undefined
  propertyName: string
  updateRecord: (propertyName: string, value: number | string) => void
}

export const HotInput: React.FC<Props> = ({
  type,
  value,
  propertyName,
  updateRecord,
}) => {
  const handleChange = ({
    target: { value },
  }: ChangeEvent<HTMLInputElement>) => {
    updateRecord(propertyName, value)
  }

  return (
    <div className="hotInput">
      <label>{propertyName}</label>
      <input value={value} type={type} onChange={handleChange}></input>
    </div>
  )
}
