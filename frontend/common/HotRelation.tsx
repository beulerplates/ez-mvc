import React, { useEffect, useState, ChangeEvent } from 'react'
import Axios from 'axios'
import { Record } from '../../server/db/entity'
import { UpdateEvent } from 'typeorm'

type Props = {
  label: string
  relationModel: string
  record: Record
  updateRecord: (field: string, value: Record) => void
}

export const HotRelation: React.FC<Props> = ({
  record,
  label,
  relationModel,
  updateRecord,
}) => {
  const [relations, setRelations] = useState<Record[]>([])

  const handleChange = (e: ChangeEvent<HTMLSelectElement>) => {
    const { id, name } = relations[parseInt(e.target.value)]
    const record = { id, name }
    updateRecord(relationModel, record)
  }

  useEffect(() => {
    const get = async () => {
      const { data } = await Axios.get(`/api/${relationModel}`)
      setRelations(data)
    }
    get()
  }, [relationModel])

  if (!record) return <div>Invalid Record. Check yo self..</div>
  if (!relations.length) return <div>Loading Relations...</div>

  return (
    <div>
      <label>{label}</label>
      <select value={record.id - 1} onChange={handleChange}>
        {relations.map(({ id, name }, i) => (
          <option key={`relation-${label}-${id}-${name}`} value={i}>
            {name}
          </option>
        ))}
      </select>
    </div>
  )
}
