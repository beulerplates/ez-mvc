import React from 'react'
import { render } from 'react-dom'
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom'

import './index.scss'

import { App } from './App/App'
import { AdminRouter } from './Admin'

const AppRouter = () => {
  return (
    <BrowserRouter>
      <div className="menu">
        <Link to="/admin">Admin</Link>
        <Link to="/">App</Link>
      </div>
      <Switch>
        <Route path="/admin">
          <AdminRouter />
        </Route>
        <Route path="/:model?">
          <App />
        </Route>
      </Switch>
    </BrowserRouter>
  )
}

render(<AppRouter />, document.getElementById('root'))
