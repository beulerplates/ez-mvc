import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { useParams } from 'react-router'
import { Link } from 'react-router-dom'
import { entities, TableMetadata } from '../../server/db/entity'
import { RecordShow } from '../Admin/RecordShow'

export const App: React.FC = ({ children }) => {
  const [records, setRecords] = useState<any[]>([])
  const [metadata, setMetadata] = useState<TableMetadata | null>(null)

  const { model } = useParams() || 'should be there'

  useEffect(() => {
    if (!metadata) return

    const getIndex = async () => {
      const { data } = await axios.get(`/api/${model}`)
      console.log(data)
      setRecords(data)
    }
    if (model) getIndex()
  }, [metadata])

  return (
    <div>
      <h1>EZ-MVC 0.3</h1>
      <div className="menu"></div>
      <h3>Main App Body</h3>
      <p>Write your functionality here...</p>
    </div>
  )
}
