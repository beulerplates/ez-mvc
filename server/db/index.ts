import {
  ConnectionOptions,
  createConnection,
  Repository,
  Connection,
} from 'typeorm'
import { User } from './entity/user'
import { Item } from './entity/item'
import { List } from './entity/list'

import { seeds } from './seeds'

export type RepoTypes = 'user' | 'item' | 'list'
const keys: RepoTypes[] = ['user', 'item', 'list']
export type ModelTypes = User | Item | List

export type Models = Repository<ModelTypes>

export class DB {
  options: ConnectionOptions
  connection?: Connection
  repos?: { [key in RepoTypes]: Models }

  constructor() {
    this.options = {
      type: 'sqlite',
      database: `./data/database.sqlite`,
      entities: [User, Item, List],
      // logging: true,
    }
  }

  connect = async () => {
    this.connection = await createConnection(this.options)
    if (!process.env.PRODUCTION) {
      console.info('Dropping database for re-creation')
      await this.connection.dropDatabase()
    }
    await this.connection.synchronize()
    this.repos = {
      user: this.connection.getRepository(User),
      item: this.connection.getRepository(Item),
      list: this.connection.getRepository(List),
    }
    console.log('DB Connected!')
  }

  reset = async () => {
    if (!this.connection || !this.repos) {
      console.error('Database is not connected!')
      return
    }

    if (!process.env.PRODUCTION) {
      console.info('>> resetting db')

      const deleteKeys: RepoTypes[] = ['list', 'user', 'item']
      for (const type of keys.slice().reverse()) {
        const repo = this.repos[type]
        console.log(`>> Purging ${type} data`)
        await repo.clear()
      }

      // murder db in between for full reset

      for (const type of keys) {
        const repo = this.repos[type]
        console.log(`>> Seeding ${type} data`)
        for (const seed of seeds[type]) {
          await repo.save(seed)
        }
      }
    }

    // test if many to many relations.
    const wanttobreakshit = false
    if (wanttobreakshit) {
      const record = seeds.list[0]
      record.items = [{ id: 9, name: 'beard trimmed' }]
      this.repos.list.update(record.id, record)
    }
  }
}
