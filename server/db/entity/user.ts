import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { List } from './list';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column('text')
  name!: string;

  @OneToMany(
    type => List,
    list => list.user
  )
  lists?: List[];
}
