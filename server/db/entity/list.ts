import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
  ManyToOne,
  JoinColumn,
} from 'typeorm'
import { Item } from './item'
import { User } from './user'

export enum ListType {
  'essentials',
  'leaving the house',
  'going to bed',
}

export const schema = {}

@Entity()
export class List {
  @PrimaryGeneratedColumn({ comment: 'generated' })
  id!: number

  @Column('text')
  name!: string

  @Column('integer', { comment: 'enum' })
  type!: ListType

  @ManyToOne(
    type => User,
    user => user.lists
  )
  user!: User

  @ManyToMany(type => Item)
  @JoinTable()
  items?: Item[]
}
