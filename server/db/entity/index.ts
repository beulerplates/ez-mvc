import { User } from './user'
import { Item } from './item'
import { List } from './list'
import { RepoTypes, ModelTypes } from '../../db'
import { EntityMetadata } from 'typeorm'

export type EntityInfo = {
  relations: string[]
  model: ModelTypes
  metadata?: EntityMetadata
  enums?: { [id: string]: string[] }
}

export type TableMetadata = {
  relations: string[]
  fields: {
    propertyName: string
    type: string
    generatedType: string
    comment: string
  }
}

export type Fields = {
  propertyName: string
  comment: string
  type?: string
}

export type Record = {
  id: number
  name: string
  type?: number
  description?: string
  relation?: Record | Record[]
}

export const entities: { [key in RepoTypes]: EntityInfo } = {
  user: { relations: ['lists'], model: User },
  item: { relations: [], model: Item },
  list: {
    relations: ['items', 'user'],
    model: List,
    enums: { type: ['essentials', 'leaving the house', 'going to bed'] },
  },
}
