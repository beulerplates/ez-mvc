import { User } from './entity/user';
import { List, ListType } from './entity/list';
import { Item } from './entity/item';

const elijah = { id: 1, name: 'Elijah' };
const kaela = { id: 2, name: 'Kaela' };

const users: User[] = [elijah, kaela];

const items: Item[] = [
  { id: 1, name: 'keys' },
  { id: 2, name: 'phone' },
  { id: 3, name: 'wallet' },
  { id: 4, name: 'teeth brushed' },
  { id: 5, name: 'laptop' },
  { id: 6, name: 'ipad' },
  { id: 7, name: 'notebook' },
  { id: 9, name: 'beard trimmed' }
];

const lists: List[] = [
  {
    id: 1,
    name: 'Everyday Essentials',
    type: ListType['essentials'],
    user: elijah,
    items: items.slice(0, 3)
  },
  {
    id: 2,
    name: 'going to work',
    type: ListType['leaving the house'],
    user: elijah,
    items: items.slice(4, 5)
  },
  {
    id: 3,
    name: 'client meeting',
    type: ListType['leaving the house'],
    user: elijah,
    items: items.slice(5, 9)
  }
];

type Seeds = {
  user: User[];
  list: List[];
  item: Item[];
};

export const seeds: Seeds = {
  user: users,
  list: lists,
  item: items
};
