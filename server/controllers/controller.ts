import express from 'express'

import { Models } from '../db'
import { Handler } from '../app'
import { EntityInfo } from '../db/entity'

interface Params {
  entity: EntityInfo
  repo: Models
  type: string
}

export class BaseController {
  public entity: EntityInfo
  public type: string
  public path: string
  public router = express.Router()
  private repo: Models
  private options: { relations: string[] }

  constructor({ repo, type, entity }: Params) {
    this.entity = entity
    this.type = type
    this.path = `/${type}`
    this.repo = repo
    this.options = { relations: entity.relations }
    console.log(`>> ${type} Controller initialized at =>`, this.path)
  }

  init = () => {
    this.router.get(`${this.path}/metadata`, this.getMetadata)
    this.router.get(`${this.path}/fields`, this.getFields)
    this.router.get(this.path, this.getAll)
    this.router.get(`${this.path}/:id`, this.getOne)
    this.router.put(`${this.path}/:id`, this.update)
    this.router.delete(`${this.path}/:id`, this.delete)
    this.router.post(`${this.path}`, this.create)
  }

  getAll: Handler = async (req, res) => {
    const data = await this.repo.find(this.options)
    res.send(data)
  }

  getOne: Handler = async ({ params: { id } }, res) => {
    const record = await this.repo.findOne(id, this.options)
    if (!record) res.send({ message: `${this.type} not found!` })
    res.send(record)
  }

  update: Handler = async ({ body, params: { id } }, res) => {
    console.log('Updating =>', id, { ...body })
    if (!body.name) {
      res.status(422)
      res.send({ error: 'body not accepted', body })
    } else {
      await this.repo.save({ ...body })
      const record = await this.repo.findOne(id, this.options)
      res.send(record)
    }
  }

  delete: Handler = async ({ params: { id } }, res) => {
    const record = await this.repo.delete(id)
    res.send(record)
  }

  create: Handler = async ({ body }, res) => {
    if (!body.name) {
      res.status(422)
      res.send({ error: 'body not accepted', body })
    } else {
      const { raw: id } = await this.repo.insert(body)
      const record = await this.repo.findOne(id, this.options)
      res.send(record)
    }
  }

  getMetadata: Handler = async (req, res) => {
    res.send({
      relations: this.entity.relations,
      enums: this.entity.enums,
      fields: this.entity.metadata?.columns.map(
        ({ propertyName, type, generatedType, comment }) => ({
          propertyName,
          type,
          generatedType,
          comment,
        })
      ),
    })
  }
  getFields: Handler = async (req, res) => {
    res.send(
      this.entity.metadata?.columns.map(
        ({ propertyName, type, generatedType, comment }) => ({
          propertyName,
          type,
          generatedType,
          comment,
        })
      )
    )
  }
}
