import express from 'express'
import multer from 'multer'
import bodyParser from 'body-parser'
import path from 'path'

import { DB } from './db'
import { BaseController } from './controllers/controller'

export type Handler = (req: express.Request, res: express.Response) => void

type AppParams = {
  db: DB
  controllers: BaseController[]
}

export class App {
  private server: express.Application
  public port = process.env.PORT || 4321
  public db: DB

  constructor({ db, controllers }: AppParams) {
    this.db = db

    this.server = express()
    this.server.use(bodyParser.urlencoded({ extended: true }))
    this.server.use(bodyParser.json())
    this.server.use(multer({ dest: 'uploads/' }).single('file'))
    this.server.use(express.static(path.join(__dirname, 'static')))

    this.server.post('/upload', ({ body, file }, res) => {
      res.send({ message: 'file uploaded!!', file: file.filename })
    })

    this.initControllers(controllers)
    this.server.get('*', (req, res) => {
      res.sendFile(path.join(__dirname, 'static/index.html'))
    })
  }

  initControllers = (controllers: BaseController[]) => {
    controllers.forEach(controller => {
      this.server.use('/api', controller.router)
    })
  }

  start = async () => {
    this.server.listen(this.port, () => console.info('server running'))
    console.log('app started:', this.port)
  }
}
