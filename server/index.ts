import { DB, RepoTypes } from './db';
import { BaseController } from './controllers/controller';
import { entities } from './db/entity';
import { App } from './app';

const main = async () => {
  const db = new DB();
  await db.connect();

  if (!db.repos) throw 'db did not connect and init, check your config, homie.';

  const controllers: BaseController[] = [];
  for (const repoName in db.repos) {

    const entity = entities[repoName as RepoTypes]
    entity.metadata = db.connection?.getMetadata(repoName)

    const controller = new BaseController({
      entity,
      repo: db.repos[repoName as RepoTypes],
      type: repoName,
    });

    // controller.getOne = (req, res) => { res.send('Overridden controller method') };
    controller.init();
    controllers.push(controller);
  }

  const app = new App({
    db,
    controllers,
  });

  await app.db.reset(); // if dev mode
  app.start();
};

main();
