# EZ-MVC

## _Prototype your app today with EZ-MVC!_

- be the talk of your town!
- get attention from your desired partner!
- become successful and rich!

## Join the EZ-MVC Team today!

### Only you can prevent forest fires!

# Description

- Just define your models in the entities folder, add them to the indexes and you get a full CRUD with a database of your choice. Enjoy!

- Also don't forget to add some seeds!

# TODO

- generate basic form views based on models wired up to the crud.
- use yml seeds for easy typeytype

# NOTE:

I hereby declare this possibly over-engineered and thus dead.... BUT it shall be reborn into a COOLER propject!  

### Lessons learned: 
- don't use TypeORM
- use Sequelize
- find better way to compile and serve static assets (it's buggy - can't see where errors really happen )